import { useSelector } from "react-redux";
import { RootState } from "store";

export const useAuth = () => {
  const { userInfo } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const { user } = useSelector((state: RootState) => state.quanLyAuthApi);
  return { userInfo, user };
};
