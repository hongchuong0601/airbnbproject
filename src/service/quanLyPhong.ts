import { apiInstance } from "constant/apiInstance";
import { Room, RoomDetail } from "types/quanLyPhong";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_PHONG_API,
});

export const quanLyPhongService = {
  getRoomList: () => api.get<ApiResponse<Room[]>>("./"),

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getRoomDetail: (roomId: any) =>
    api.get<ApiResponse<RoomDetail>>(`./${roomId}`),

  postRoomList: () => api.get('./'),
    
 // eslint-disable-next-line @typescript-eslint/no-explicit-any
  deleteRoom: (id: any) => api.delete(`./${id}`),
};
