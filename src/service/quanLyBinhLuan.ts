import { apiInstance } from "constant";
import { binhLuanPhong } from "types";

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_BINH_LUAN_API,
})

export const quanLyBinhLuanService = {
    getBinhLuanPhong: (roomId: string)=>api.get<ApiResponse<binhLuanPhong[]>>(`./lay-binh-luan-theo-phong/${roomId}`),

  
}