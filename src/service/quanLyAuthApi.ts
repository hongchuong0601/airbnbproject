import { apiInstance } from "constant/apiInstance";
import { SigninSchemaType, SignupSchemaType } from "schema";
import { User } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_AUTH_API,
});

export const quanLyAuthApiService = {
  register: (payload: SignupSchemaType) => api.post("./signup", payload),

  login: (payload: SigninSchemaType) =>
    api.post<ApiResponse<User>>("./signin", payload),
};
