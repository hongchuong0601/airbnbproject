import { apiInstance } from "constant";
import { AccountSchemaType } from "schema";
import { UserInfo } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API,
});

export const quanLyNguoiDungService = {
  getUserInfo: (id: string) => api.get<ApiResponse<UserInfo>>(`./${id}`),

  updateUser: (id: string, payload: AccountSchemaType) =>
    api.put<ApiResponse<UserInfo>>(`./${id}`, payload),

  getAllUser: () => api.get<ApiResponse<UserInfo[]>>("./"),

  deleteUser: (delId) => api.delete<ApiResponse<UserInfo>>(`./?id=${delId}`),

  postAvatar: () => api.post('./upload-avatar')
};
