import { apiInstance } from "constant";
import { AllBooking, UserBooking } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_DAT_PHONG_API,
});

export const quanLyDatPhongService = {
  getAllBooking: () => api.get<ApiResponse<AllBooking[]>>("./"),

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getUserBooking: (maNguoiDung: any) => api.get<ApiResponse<UserBooking[]>>(`./lay-theo-nguoi-dung/${maNguoiDung}`),

  postUserBooking: (payload) => api.post<ApiResponse<UserBooking>>('./', payload),

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  deleteUserBooking: (id: any)=> api.delete<ApiResponse<UserBooking[]>>(`./${id}`)


};
