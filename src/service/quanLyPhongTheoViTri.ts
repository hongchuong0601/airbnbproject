import { apiInstance } from "constant"
import { location } from "types"

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_PHONG_THEO_VI_TRI_API
})

export const quanLyPhongTheoViTriService = {
    getAllLocation:  () => api.get<ApiResponse<location[]>>('./'),

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    deleteLocation: (id: any) => api.delete(`./${id}`),
}