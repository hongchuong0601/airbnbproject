export interface AllBooking {
    key?: number;
    id: number;
    maPhong: number;
    ngayDen: string;
    ngayDi: string;
    soLuongKhach:number ;
    maNguoiDung: number;
}

export interface UserBooking {
    key?: number;
    id: number;
    maPhong: number;
    ngayDen: string;
    ngayDi: string;
    soLuongKhach:number ;
    maNguoiDung: number;
}