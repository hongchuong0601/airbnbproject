import { AccountTemplate } from "components/templates/account";

export const Account = () => {
  return (
    <div>
      <AccountTemplate />
    </div>
  );
};

export default Account;
