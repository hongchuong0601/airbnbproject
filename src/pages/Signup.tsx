import { SignupTemplate } from "components/templates";

export const Signup = () => {
  return (
    <div>
      <SignupTemplate />
    </div>
  );
};

export default Signup;
