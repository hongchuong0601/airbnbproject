import { AdminTemplate } from "components/templates";

export const Admin = () => {
  return <AdminTemplate />;
};

export default Admin;
