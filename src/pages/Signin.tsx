import { SigninTemplate } from "components/templates";

export const Signin = () => {
  return (
    <div>
      <SigninTemplate />
    </div>
  );
};

export default Signin;
