import { DetailTemplate } from "components/templates";

export const Detail = () => {
  return (
    <div>
      <DetailTemplate />
    </div>
  );
};

export default Detail;
