import { AuthLayout, MainLayout } from "components/layouts";
import { ThemNguoiDung } from "components/templates/AdminTemplate/QuanLyNguoiDung";
import { PATH } from "constant/config";
import { Account, Admin, Detail, Home, Signin, Signup } from "pages";
import { RouteObject } from "react-router-dom";

console.log("PATH:", PATH);
export const router: RouteObject[] = [
  // {
  //   path: "/admin",
  //   element: <Admin />,
  //   children: [
  //     {}
  //   ]
  // },
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: PATH.detail,
        element: <Detail />,
      },
      {
        path: PATH.account,
        element: <Account />,
      },
      {
        path: PATH.admin,
        element: <Admin />,
      },
      {
        path: PATH.addUser,
        element: <ThemNguoiDung />,
      },
    ],
  },
  {
    element: <AuthLayout />,
    children: [
      {
        path: PATH.signin,
        element: <Signin />,
      },
      {
        path: PATH.signup,
        element: <Signup />,
      },
    ],
  },
];
