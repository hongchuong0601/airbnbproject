export const PATH = {
    signin: '/login',
    signup: '/register',
    account: '/account',
    detail: '/detail/:roomId',
    admin: '/admin',
    addUser: '/addUser'
}