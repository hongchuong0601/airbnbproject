import styled from "styled-components";
import { Popover } from ".";

export const Footer = () => {
  return (
    <FooterS>
      <div className="footer-content pb-10 pt-20">
        <Popover
          content={
            <div className="p-10">
              <ul>
                <li className="!mb-10 ">
                  <a href="#" className="!text-black !dark:hover:text-blue-600">
                    Phương thức hoạt động của Airbnb
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Trang tin tức
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Nhà đầu tư
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Airbnb Plus
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Airbnb luxe
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    HotelTonight
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Airbnb for Work
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Nhờ có Host, mọi điều đều có thể
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Cơ hội nghề nghiệp
                  </a>{" "}
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Thư của nhà sáng lập
                  </a>
                </li>
              </ul>
            </div>
          }
          placement="topLeft"
        >
          <p className="font-600 text-xl  dark:hover:text-blue-600 transition-all max-[700px]:text-sm">
            Giới thiệu về chúng tôi
          </p>
        </Popover>
        <Popover
          content={
            <div className="p-10">
              <ul>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Sự đa dạng và Cảm giác thân thuộc
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Tiện nghi phù hợp cho người khuyết tật
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Đối tác liên kết Airbnb
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Chổ ở cho tuyển đấu
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Lượt giới thiệu của khách
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Airbnb.org
                  </a>
                </li>
              </ul>
            </div>
          }
          placement="topLeft"
        >
          <p className="font-600 text-xl dark:hover:text-blue-600 transition-all max-[700px]:text-sm">
            Cộng đồng trao đổi
          </p>
        </Popover>
        <Popover
          content={
            <div className="p-10">
              <ul>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Cho thuê nhà
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Tổ chức trải nghiệm trực tuyến
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Tổ chức trải nghiệm
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Đón tiếp khách có trách nhiệm
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Trung tâm tài nguyên
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Trung tâm cộng đồng
                  </a>
                </li>
              </ul>
            </div>
          }
          placement="topRight"
        >
          <p className="font-600 text-xl  dark:hover:text-blue-600 transition-all max-[700px]:text-sm">
            Đón tiếp khách
          </p>
        </Popover>
        <Popover
          content={
            <div className="p-10">
              <ul>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Biện pháp ứng phó với đại dịch
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    COVID-19 của chúng tôi
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Trung tâm trợ giúp
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Các tùy chọn hủy
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Hỗ trợ khu dân cư
                  </a>
                </li>
                <li className="!mb-10">
                  <a href="#" className="!text-black">
                    Tin cậy và an toàn
                  </a>
                </li>
              </ul>
            </div>
          }
          placement="topRight"
        >
          <p className="font-600 text-xl  dark:hover:text-blue-600 transition-all max-[700px]:text-sm">
            Hỗ trợ khách hàng
          </p>
        </Popover>
      </div>
      <div className="mx-auto w-full max-w-screen-xl p-4 py-6 lg:py-8 ">
        <hr className="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
        <div className=" footer-content">
          <span className="text-xl text-gray-500 hover:text-gray-900 dark:hover:text-red-600 max-[700px]:text-sm">
            © 2023{" "}
            <a
              href="https://flowbite.com/"
              className="hover:underline max-[700px]:text-sm"
            >
              AirBnb
            </a>
            . All Rights Reserved.
          </span>
          <div className="flex  space-x-5 sm:justify-center sm:mt-0 ">
            <a
              href="#"
              className="text-gray-500 hover:text-gray-900 dark:hover:text-red-600 mr-10"
            >
              <i className="fa fa-globe mr-10 text-20 max-[700px]:text-sm"></i>
              <span className="text-20 text-center max-[700px]:text-sm">
                Tiếng Việt
              </span>
            </a>
            <a
              href="#"
              className="text-gray-500 hover:text-gray-900 dark:hover:text-red-600"
            >
              <i className="fab fa-facebook mr-10 text-blue-600-600 text-20 max-[700px]:text-sm"></i>
            </a>
            <a
              href="#"
              className="text-gray-500 hover:text-gray-900 dark:hover:text-red-600 "
            >
              <i className="fab fa-twitter mr-10 text-20 max-[700px]:text-sm"></i>
            </a>
            <a
              href="#"
              className="text-gray-500 hover:text-gray-900 dark:hover:text-red-600"
            >
              <i className="fab fa-instagram mr-10 text-20 max-[700px]:text-sm"></i>
            </a>
          </div>
        </div>
      </div>
    </FooterS>
  );
};

export default Footer;

const FooterS = styled.footer`
  height: var(--footer-height);
  line-height: 30px;
  background: black;
  color: white;
  margin-top: 50px;
  .footer-content {
    max-width: var(--max-width);
    margin-top: 40px;
    margin: auto;
    display: flex;
    justify-content: space-between;
    height: 100%;
  }
`;
