import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Popover } from ".";
import { UserOutlined, MenuOutlined } from "@ant-design/icons";
import { useAppDispatch } from "store";
import { useAuth } from "hook";
import { PATH } from "constant";
import { quanLyAuthApiActions } from "store/authApi/slice";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

// import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

export const Header = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { userInfo } = useAuth();

  return (
    <HeaderS>
      <div className="header-content ">
        <div
          className="flex items-center justify-center cursor-pointer "
          onClick={() => {
            navigate("/");
          }}
        >
          <i className="fab fa-airbnb mr-10 font-700 text-orange-600 text-3xl max-[600px]:text-2xl max-[300px]:text-xl "></i>
          <h2 className="font-600  text-[30px] max-[600px]:text-2xl  max-[350px]:hidden">
            CYBER AIRBNB
          </h2>
        </div>
        <div className="flex justify-around items-center">
          <div className="flex justify-around items-center max-[750px]:hidden ">
            {userInfo !== undefined && userInfo.role === "ADMIN" && (
              <NavLink to={PATH.admin} className="mr-40">
                Admin
              </NavLink>
            )}
            <NavLink to="" className="mr-40">
              About
            </NavLink>
            <NavLink to="">Contact</NavLink>
            {userInfo && (
              <Popover
                content={
                  <div className="p-10">
                    <h2 className="font-900">{userInfo.name}</h2>
                    <hr />
                    <div
                      className=" mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                      onClick={() => {
                        navigate(PATH.account);
                      }}
                    >
                      Thông tin tài khoản
                    </div>
                    <div
                      className=" mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                      onClick={() => {
                        dispatch(quanLyAuthApiActions.logOut());
                        dispatch(quanLyNguoiDungActions.logOut());
                      }}
                    >
                      Đăng xuất
                    </div>
                  </div>
                }
                placement="topRight"
              >
                <Avatar
                  className="!ml-40 cursor-pointer !flex !items-center !justify-center !bg-black"
                  size={35}
                  icon={<UserOutlined />}
                />
              </Popover>
            )}
            {!userInfo && (
              <p
                className="font-600 text-16 ml-40 cursor-pointer"
                onClick={() => {
                  navigate(PATH.signin);
                }}
              >
                Login
              </p>
            )}
          </div>
          <div className=" min-[750px]:hidden max-[750px]:text-sm">
            {userInfo && (
              <Popover
                content={
                  <div className="p-10 ">
                    <Avatar
                      className="!mb-10 cursor-pointer !flex !items-center !justify-center !bg-black"
                      size={35}
                      icon={<UserOutlined />}
                      onClick={() => {
                        navigate(PATH.account);
                      }}
                    />
                    <hr />
                    {userInfo !== undefined && userInfo.role === "ADMIN" && (
                      <div
                        className=" mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                        onClick={() => {
                          navigate(PATH.admin);
                        }}
                      >
                        Admin
                      </div>
                    )}
                    <div className=" mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300">
                      Contact
                    </div>
                    <div className=" mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300">
                      About
                    </div>
                    <hr />
                    <div
                      className=" mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                      onClick={() => {
                        dispatch(quanLyAuthApiActions.logOut());
                        dispatch(quanLyNguoiDungActions.logOut());
                      }}
                    >
                      Đăng xuất
                    </div>
                  </div>
                }
                placement="topRight"
              >
                {/* làm Icon người dùng đăng nhập vào  */}
                <Avatar
                  className="!ml-40 cursor-pointer !flex !items-center !justify-center !text-black !bg-white "
                  size={35}
                  icon={<MenuOutlined className="text-xl" />}
                />
              </Popover>
            )}
            {!userInfo && (
              <p
                className="font-600 text-16 ml-40 cursor-pointer"
                onClick={() => {
                  navigate(PATH.signin);
                }}
              >
                Login
              </p>
            )}
          </div>
        </div>
      </div>
    </HeaderS>
  );
};

export default Header;

// tạo ra 1 component HeaderS
const HeaderS = styled.header`
  height: var(--max-height);
  background: white;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  /* position: absolute; */
  /* top: 0; */
  /* width: 100vw; */
  .header-content {
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    z-index: 10;
  }
`;
