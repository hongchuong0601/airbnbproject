import { HTMLInputTypeAttribute } from "react";
import { UseFormRegister } from "react-hook-form";

type SelectProps = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
  type?: HTMLInputTypeAttribute;
  label?: string;
  className?: string;
  disabled?: boolean;
};

export const Select = ({
  register,
  error,
  name,
  className,
  disabled,
}: SelectProps) => {
  return (
    <div className={className}>
      {/* {label && <p className="mb-10">{label}</p>} */}
      <select
        disabled={disabled}
        defaultValue={"DEFAULT"}
        className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        {...register(name)}
      >
        <option value="DEFAULT" disabled>
          Chọn giới tính
        </option>
        <option value="true">Male</option>
        <option value="false">Female</option>
      </select>
      {error && <p className="text-red-500">{error}</p>}
    </div>
  );
};

export default Select;
