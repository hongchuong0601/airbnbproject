import { PATH } from "constant/config";
import { useNavigate } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import { SigninSchema, SigninSchemaType } from "schema";
import { zodResolver } from "@hookform/resolvers/zod";
import { toast } from "react-toastify";
import { Input } from "components/ui";
import { useAppDispatch } from "store";
import { signinThunk } from "store/authApi/thunk";

export const SigninTemplate = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<SigninSchemaType>({
    mode: "onChange",
    resolver: zodResolver(SigninSchema),
  });

  const onSubmit: SubmitHandler<SigninSchemaType> = async (value) => {
    console.log("value:", value);
    dispatch(signinThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng nhập thành công");
        navigate("/");
        location.reload();
      })
      .catch(() => {
        toast.error("Email hoặc Password không đúng");
      });
  };

  return (
    <div>
      <form className="pt-[30px] pb-[60px]" onSubmit={handleSubmit(onSubmit)}>
        <h2 className="text-white text-40 font-600">Sign In</h2>
        <div className="mt-40">
          <Input
            register={register}
            name="email"
            error={errors?.email?.message}
            placeholder="Email"
            type="email"
          />
        </div>
        <div className="mt-40">
          <Input
            register={register}
            name="password"
            error={errors?.password?.message}
            placeholder="Password"
            type="password"
          />
        </div>
        <div className="mt-40">
          <button className="text-white w-full bg-red-700 font-500 rounded-lg text-20 px-5 py-[16px]">
            Sign In
          </button>
          <p className="mt-10 text-white">
            Chưa có tài khoản?
            <span
              className="text-blue-500 cursor-pointer"
              onClick={() => {
                navigate(PATH.signup);
              }}
            >
              Sign up
            </span>
          </p>
        </div>
      </form>
    </div>
  );
};

export default SigninTemplate;
