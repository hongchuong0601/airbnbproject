import { zodResolver } from "@hookform/resolvers/zod/src/zod.js";
import { Button, Input } from "components/ui";
import { useAuth } from "hook";
import { useEffect } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { AccountSchema, AccountSchemaType } from "schema";
import { RootState, useAppDispatch } from "store";
import { postAvatarThunk, updateUserThunk } from "store/quanLyNguoiDung/thunk";
import { styled } from "styled-components";
import { useState } from "react";
import { Modal } from "antd";

const AccountInfoTab = () => {
  const { userInfo } = useAuth();
  console.log("userInfo:", userInfo);
  const dispatch = useAppDispatch();
  const { isUpdatingUser } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<AccountSchemaType>({
    mode: "onChange",
    resolver: zodResolver(AccountSchema),
  });
  console.log("errors:", errors);

  // dùng để show thông tin người dùng vào các ô input
  useEffect(() => {
    return reset({
      ...userInfo,
    });
  }, [userInfo, reset]);
  console.log("userAccount:", userInfo);
  const onSubmit: SubmitHandler<AccountSchemaType> = (value) => {
    dispatch(updateUserThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Cập nhật thông tin tài khoản thành công");
      })
      .catch(() => {
        toast.error("Cập nhật thông tin tài khoản thất bại");
      });
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
    dispatch(postAvatarThunk());
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className="mb-100">
      <div className="px-40">
        <img
          className="w-150 h-150 rounded-full"
          src="/images/jongxina.jpg"
          alt="Rounded avatar"
        />
        <div className="mt-10 ml-10">
          <Button type="primary" onClick={showModal}>
            Thay đổi avatar
          </Button>
          <Modal
            title="Thay đổi Avatar"
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
          >
            <label
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              // for="file_input"
            >
              Upload file
            </label>
            <input
              className="block w-full text-xl text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
              id="file_input"
              type="file"
            />
          </Modal>
        </div>
      </div>
      <form className="px-40" onSubmit={handleSubmit(onSubmit)}>
        <InputS disabled label="Id" name="id" register={register} type="text" />
        <InputS
          label="Họ và tên"
          name="name"
          register={register}
          error={errors?.name?.message}
        />
        <InputS
          label="Email"
          name="email"
          register={register}
          error={errors?.email?.message}
        />
        <InputS
          label="Số điện thoại"
          name="phone"
          register={register}
          error={errors?.phone?.message}
        />
        <InputS
          type="date"
          label="Ngày sinh"
          name="birthday"
          register={register}
          error={errors?.birthday?.message}
        />
        <div className="mt-20 ">
          <label
            // for="countries"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
          >
            Select an option
          </label>
          <select
            name="gender"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-black border border-white rounded-lg bg-white sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-white dark:border-gray-600 dark:placeholder-white  mt-10  "
            {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Chọn giới tính
            </option>
            <option value="true">Male</option>
            <option value="false">Female</option>
          </select>
          {errors?.gender && (
            <p className="text-red-500">{errors?.gender?.message as string}</p>
          )}
        </div>
        <InputS
          disabled
          label="Role"
          name="role"
          register={register}
          error={errors?.role?.message}
        />
        <div className="text-right">
          <Button
            htmlType="submit"
            className="mt-[60px] w-[200px] !h-[50px]"
            type="primary"
            loading={isUpdatingUser}
          >
            Lưu thay đổi
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AccountInfoTab;

const InputS = styled(Input)`
  margin-top: 20px;
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
