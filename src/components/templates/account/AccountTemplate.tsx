import { Tabs } from "components/ui";
import AccountInfoTab from "./AccountInfoTab";
import AccountBooking from "./AccountBooking";
import { useAuth } from "hook";
import { Navigate } from "react-router-dom";

export const AccountTemplate = () => {
  const { userInfo } = useAuth();
  console.log("AccountuserInfo:", userInfo);

  if (!localStorage.getItem("Id")) {
    alert("Bạn phải đăng nhập mới vào được trang này");
    return <Navigate to="../" />;
  }
  return (
    <div>
      <Tabs
        tabPosition="left"
        className="h-full"
        tabBarGutter={-5}
        items={[
          {
            label: (
              <div className="w-[150px] text-left hover:bg-gray-400 hover:text-white rounded-lg transition-all p-10 text-black">
                Thông tin tài khoản
              </div>
            ),
            key: "accountInfo",
            children: <AccountInfoTab />,
          },
          {
            label: (
              <div className="w-[150px] text-left hover:bg-gray-400 hover:text-white rounded-lg transition-all p-10 text-black">
                Thông tin phòng đã đặt
              </div>
            ),
            key: "ticketInfo",
            // children: <div className="min-h-full">show thông tin</div>,
            children: <AccountBooking />,
          },
        ]}
      />
    </div>
  );
};

export default AccountTemplate;
