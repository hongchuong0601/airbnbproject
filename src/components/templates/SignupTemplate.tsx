import { zodResolver } from "@hookform/resolvers/zod/src/zod.js";
import { useForm } from "react-hook-form";
import { SignupSchema, SignupSchemaType } from "schema";
import { SubmitHandler } from "react-hook-form";
import { Input } from "components/ui";
import { useAppDispatch } from "store";
import { signupThunk } from "store/authApi/thunk";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant/config";

export const SignupTemplate = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<SignupSchemaType>({
    mode: "onChange",
    resolver: zodResolver(SignupSchema),
  });

  const onSubmit: SubmitHandler<SignupSchemaType> = async (value) => {
    if (value.gender === "male") {
      value.gender = true;
    } else {
      value.gender = false;
    }
    dispatch(signupThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng ký thành công");
        navigate(PATH.signin);
      });
  };

  return (
    <form
      className="pt-[10px] pb-[20px] w-full"
      onSubmit={handleSubmit(onSubmit)}
    >
      <h2 className="text-white text-40 font-600">Sign up</h2>
      <div className="grid grid-cols-2 gap-10 max-[1000px]:grid-cols-3 min-[2000px]:grid-cols-1">
        <div className="mt-10 ">
          <Input
            register={register}
            name="id"
            error={errors?.id?.message}
            placeholder="ID"
            type="number"
          />
        </div>
        <div className="mt-10">
          <Input
            register={register}
            name="name"
            error={errors?.name?.message}
            placeholder="Name"
            type="text"
          />
        </div>
        <div className="mt-10">
          <Input
            register={register}
            name="email"
            error={errors?.email?.message}
            placeholder="Email"
            type="text"
          />
        </div>
        <div className="mt-10">
          <Input
            register={register}
            name="password"
            error={errors?.password?.message}
            placeholder="Password"
            type="password"
          />
        </div>
        <div className="mt-10">
          <Input
            register={register}
            name="phone"
            error={errors?.phone?.message}
            placeholder="Phone"
            type="tel"
          />
        </div>
        <div className="mt-10">
          <Input
            register={register}
            name="birthday"
            error={errors?.birthday?.message}
            placeholder="Birthday"
            type=""
          />
        </div>
        <div className="mt-10">
          <select
            name="gender"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Chọn giới tính
            </option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        </div>
        <div className="mt-10">
          <Input
            register={register}
            name="role"
            error={errors?.role?.message}
            placeholder="Role"
            type="text"
          />
        </div>
      </div>
      <div className="mt-20">
        <button className="text-white w-full bg-red-700 font-500 rounded-lg text-20 px-5 py-[16px]">
          Sign Up
        </button>
        <p className="mt-10 text-white">
          Đã có tài khoản?
          <span
            className="text-blue-500 cursor-pointer"
            onClick={() => {
              navigate(PATH.signin);
            }}
          >
            Sign in
          </span>
        </p>
      </div>
    </form>
  );
};

export default SignupTemplate;
