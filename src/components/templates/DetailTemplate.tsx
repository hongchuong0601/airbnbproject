import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getBinhLuanPhongThunk } from "store/quanLyBinhLuan/thunk";
import { getRoomDetailThunk } from "store/quanLyPhong/thunk";
import { useForm } from "react-hook-form";
import { postUserBookingThunk } from "store/quanLyDatPhong/thunk";
import { toast } from "react-toastify";
import { useAuth } from "hook";

export const DetailTemplate = () => {
  const { userInfo } = useAuth();
  const dispatch = useAppDispatch();
  const params = useParams();
  const { roomId } = params;

  const { detailList } = useSelector(
    (state: RootState) => state.quanLyPhongApi
  );
  console.log("detailList:", detailList);

  const { binhLuanPhong } = useSelector(
    (state: RootState) => state.quanLyBinhLuan
  );
  console.log("binhLuanPhong:", binhLuanPhong);

  useEffect(() => {
    dispatch(getRoomDetailThunk(roomId));
  }, [dispatch, roomId]);

  useEffect(() => {
    dispatch(getBinhLuanPhongThunk(roomId));
  }, [dispatch, roomId]);

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    mode: "onChange",
  });

  const onSubmit = async (value) => {
    value = {
      ...value,
      id: "",
      maPhong: detailList.id,
      maNguoiDung: userInfo.id,
    };
    console.log("value", value);
    dispatch(postUserBookingThunk())
      .unwrap()
      .then(() => {
        toast.success("Đặt phòng thành công");
      });
  };

  return (
    <div>
      <img className="object-cover md:w-100" src={detailList?.hinhAnh} alt="" />
      <div className="min-[800px]:flex min-[800px]:row">
        <div className="flex flex-col justify-between leading-normal col-span-8">
          <p className=" font-700 text-black text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3 mt-20">
            {detailList?.tenPhong}
          </p>
          <p className="mb-20 font-400 text-black text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
            <span className="min-[800px]:mb-20 font-400 text-black text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
              {detailList?.khach} khách
            </span>{" "}
            -{" "}
            <span className="min-[800px]:mb-20 font-400 text-black text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
              {detailList?.phongNgu} phòng ngủ
            </span>{" "}
            -{" "}
            <span className="min-[800px]:mb-20 font-400 text-black text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
              {detailList?.giuong} giường
            </span>{" "}
            -{" "}
            <span className="min-[800px]:mb-20 font-400 text-black text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
              {detailList?.phongTam} phòng tắm
            </span>
          </p>
          <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-400" />
          <div className="">
            <div className="flex items-start min-[800px]:mt-20 max-[250px]:leading-3">
              <i className="fa fa-arrow-right text-xl mr-20 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3"></i>
              <div className="max-[250px]:leading-3">
                <h2 className="font-600 text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                  Toàn bộ nhà
                </h2>
                <p className="text-gray-600 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                  Bạn sẽ có chung cư cao cấp cho riêng mình
                </p>
              </div>
            </div>
            <div className="flex items-start min-[800px]:mt-20">
              <i className="fa fa-arrow-right text-xl mr-20 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3"></i>
              <div className="">
                <h2 className="font-600 text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                  Vệ sinh tăng cường
                </h2>
                <p className="text-gray-600 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                  Chủ nhà này đã cam kết thực hiện quy trình vệ sinh tăng cường
                  5 bước của Airbnb.{" "}
                  <span className="font-700 cursor-pointer underline max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                    Hiển thị thêm
                  </span>
                </p>
              </div>
            </div>
            <div className="flex min-[600px]:mr-10 items-start  min-[800px]:mt-20 ">
              <i className="fa fa-arrow-right text-xl mr-20  max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3"></i>
              <div className="">
                <h2 className="font-600 text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                  Phòng là chủ nhà siêu cấp
                </h2>
                <p className="text-gray-600 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                  Chủ nhà siêu cấp là những chủ nhà có kinh nghiệm, được đánh
                  giá cao và là những người cam kết mang lại quảng thời gian ở
                  tuyệt vời cho khách
                </p>
              </div>
            </div>
            <div className="flex items-start min-[800px]:mt-20">
              <i className="fa fa-arrow-right text-xl mr-20 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3"></i>
              <h2 className="font-600 text-xl max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10 max-[250px]:leading-3">
                Miễn phí hủy trong 48H
              </h2>
            </div>
            <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-400 mt-40 min-[800px]:mt-20" />
            <div className="">
              <textarea
                rows={4}
                className="block p-10 w-full text-xl text-black bg-gray-100 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-300 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 mt-20 max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10"
                placeholder="Dịch sang Tiếng Việt"
              />
              <p className="min-[800px]:mt-20 text-xl text-gray-700">
                {detailList.moTa}
              </p>
            </div>
          </div>
          <div className="min-[800px]:mt-20">
            <h2 className="font-700 text-2xl min-[800px]:mb-20 underline">
              Tiện nghi có sẵn
            </h2>
            <div className="grid grid-cols-2 gap-20">
              {detailList.tivi === true && (
                <div className="text-xl ">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Tivi với truyền hình cáp 8K</span>
                </div>
              )}
              {detailList.dieuHoa === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Điều hòa thông minh giúp lọc không khí</span>
                </div>
              )}
              {detailList.wifi === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Wifi 5G</span>
                </div>
              )}
              {detailList.mayGiat === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Máy giặt thông minh có sấy khô quần áo</span>
                </div>
              )}
              {detailList.hoBoi === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Hồ bơi rộng thoáng</span>
                </div>
              )}
              {detailList.doXe === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Chổ đậu xe cho xe hơi</span>
                </div>
              )}
              {detailList.banLa === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Bàn là</span>
                </div>
              )}
              {detailList.banUi === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Bàn ủi</span>
                </div>
              )}
              {detailList.bep === true && (
                <div className="text-xl">
                  <i className="fa fa-arrow-circle-right"></i>{" "}
                  <span>Bếp có nhiều đồ dùng giúp nấu ăn</span>
                </div>
              )}
            </div>

            <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-400" />
          </div>
        </div>
        <div className="col-span-4 ">
          <div className="border rounded-lg border-gray-300 pt-20 pb-20 min-[800px]:ml-20 min-[800px]:mt-20 ">
            <div>
              <div className="flex justify-between min-[800px]:mb-20 min-[800px]:mt-10 items-center">
                <p className="pl-50 text-xl font-700 max-[960px]:text-sm">
                  $ {detailList.giaTien} / đêm
                </p>
                <p className="pr-50 max-[960px]:text-sm">
                  18 like <span className="underline">( đánh giá )</span>
                </p>
              </div>
              <form
                className="min-[800px]:ml-50 min-[800px]:mr-50"
                onSubmit={handleSubmit(onSubmit)}
              >
                <div className="grid md:grid-cols-2 ">
                  <div className="w-full">
                    <input
                      type="datetime-local"
                      id="ngayDen"
                      name="ngayDen"
                      // className="bg-gray-50 border border-gray-300 text-gray-900 text-sm  focus:ring-blue-500 w-full px-3 py-20"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Ngày nhận phòng"
                      {...register("ngayDen")}
                    />
                    <p className="text-red-500">
                      {errors?.ngayDen?.message as string}
                    </p>
                  </div>
                  <div>
                    <input
                      type="datetime-local"
                      name="ngayDen"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Ngày trả phòng"
                      {...register("ngayDi")}
                    />
                    <p className="text-red-500">
                      {errors?.ngayDi?.message as string}
                    </p>
                  </div>
                </div>
                <div className="mb-6 w-full">
                  <input
                    type="text"
                    id="first_name"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Số khách"
                    {...register("khach")}
                  />
                  <p className="text-red-500">
                    {errors?.ngayDi?.message as string}
                  </p>
                </div>
                <button className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full  px-5 py-15 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800 mt-15">
                  Đặt Phòng
                </button>
                <p className="mt-15 text-center">Bạn vẫn chưa bị trừ tiền</p>
              </form>
            </div>
            <div className="relative overflow-x-auto mx-20 ">
              <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 mt-10">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50  dark:text-gray-700">
                  <tr>
                    <th scope="col" className="px-6 py-3 ">
                      Dịch vụ
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Số ngày
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Giá
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="bg-white dark:border-gray-700 ">
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-black"
                    >
                      Tiền phòng
                    </th>
                    <td className="px-6 py-4 text-center">1</td>
                    <td className="px-6 py-4">$ 220</td>
                  </tr>
                  <tr className="bg-white border-b  dark:border-gray-700">
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-black"
                    >
                      Phí dịch vụ
                    </th>
                    <td className="px-6 py-4 text-center"></td>
                    <td className="px-6 py-4">$ 22</td>
                  </tr>
                  <tr className="bg-white   dark:border-black">
                    <th
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-black "
                    >
                      Tổng
                    </th>
                    <td className="px-6 py-4 text-center"></td>
                    <td className="px-6 py-4">$ 220</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-2">
        {binhLuanPhong.map((binhLuan) => {
          return (
            <div className="pt-20 pb-20 " key={binhLuan.id}>
              <div className="flex items-center mb-10 mr-20">
                <img
                  src={binhLuan.avatar}
                  alt="123"
                  className="rounded-full w-40 h-40 bg-black mr-10"
                />
                <div>
                  <h2 className="font-700">{binhLuan.tenNguoiBinhLuan}</h2>
                  <p>{binhLuan.ngayBinhLuan}</p>
                </div>
              </div>
              <div>
                <p className="font-600 text-xl">{binhLuan.noiDung}</p>
              </div>
            </div>
          );
        })}
      </div>
      <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-400" />
      <div>
        <form>
          <div className="w-full mb-4 border border-gray-200 rounded-lg bg-gray-50 dark:bg-gray-300 dark:border-gray-600">
            <div className="px-4 py-2 bg-white rounded-t-lg dark:bg-gray-800">
              <label htmlFor="comment" className="sr-only">
                Your comment
              </label>
              <textarea
                id="comment"
                rows={4}
                className="w-full p-10 text-xl text-gray-700 bg-white border-0 dark:bg-gray-300 focus:ring-0 dark:text-gray-700 dark:placeholder-gray-700 font-600"
                placeholder="Write a comment..."
                required
              ></textarea>
            </div>
            <div className=" px-3 py-2 border-t dark:border-gray-600">
              <button
                type="submit"
                className="inline-flex items-center py-2.5 px-20 text-xl font-medium text-center text-white bg-blue-500 rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-blue-800"
              >
                Add comment
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default DetailTemplate;
