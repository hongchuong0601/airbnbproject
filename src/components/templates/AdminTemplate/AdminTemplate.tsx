import { Tabs } from "components/ui";
import { AdminQuanLyNguoiDung } from "./QuanLyNguoiDung";
import { Navigate } from "react-router-dom";
import { useAuth } from "hook";
import { AdminQuanLyPhong } from "./QuanLyPhong";
import { AdminQuanLyDatPhong } from "./QuanLyDatPhong";
import { AdminQuanLyThongTinViTri } from "./QuanLyThongTinViTri";

export const AdminTemplate = () => {
  const { userInfo } = useAuth();

  if (!localStorage.getItem("Id")) {
    alert("Bạn không quyền truy cập vào trang này");
    return <Navigate to="../" />;
  }

  if (userInfo !== undefined && userInfo?.role !== "ADMIN") {
    alert("Bạn không quyền truy cập vào trang này");
    return <Navigate to="../" />;
  }

  return (
    <div>
      <Tabs
        tabPosition="left"
        className="h-full"
        tabBarGutter={-5}
        items={[
          {
            label: (
              <div className="w-[150px]  text-left hover:bg-gray-700 hover:text-white rounded-lg transition-all p-10 text-black ">
                Quản lý người dùng
              </div>
            ),
            key: "adminQuanLyNguoiDung",
            children: <AdminQuanLyNguoiDung />,
          },
          {
            label: (
              <div className="w-[150px] text-left hover:bg-gray-700 hover:text-white rounded-lg transition-all p-10 text-black">
                Quản lý phòng
              </div>
            ),
            key: "adminQuanLyPhong",
            children: <AdminQuanLyPhong />,
          },
          {
            label: (
              <div className="w-[150px] text-left hover:bg-gray-700 hover:text-white rounded-lg transition-all p-10 text-black">
                Quản lý đặt phòng
              </div>
            ),
            key: "adminQuanLyDatPhong",
            children: <AdminQuanLyDatPhong />,
          },
          {
            label: (
              <div className="w-[150px] text-left hover:bg-gray-700 hover:text-white rounded-lg transition-all p-10 text-black">
                Quản lý thông tin vị trí
              </div>
            ),
            key: "adminQuanLyThongTinViTri",
            children: <AdminQuanLyThongTinViTri />,
          },
        ]}
      />
    </div>
  );
};

export default AdminTemplate;
