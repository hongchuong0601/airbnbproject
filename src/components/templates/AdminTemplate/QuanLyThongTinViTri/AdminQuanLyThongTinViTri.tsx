import { useEffect } from "react";
import { Button } from "components/ui";
import { Input, Table } from "antd";
import { AudioOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { location } from "types";
import type { ColumnsType, TableProps } from "antd/es/table";
import {
  deleteLocationThunk,
  getAllLocationThunk,
} from "store/quanLyPhongTheoViTri/thunk";

export const AdminQuanLyThongTinViTri = () => {
  const dispatch = useAppDispatch();
  const { locationList } = useSelector(
    (state: RootState) => state.quanLyPhongTheoViTri
  );

  console.log("locationList:", locationList);
  useEffect(() => {
    dispatch(getAllLocationThunk());
  }, [dispatch]);
  // search antd
  const { Search } = Input;
  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#1677ff",
      }}
    />
  );
  const onSearch = (value, _e, info) => console.log(info?.source, value);

  // table antd
  const columns: ColumnsType<location> = [
    {
      title: "Id",
      dataIndex: "id",
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend"],
      width: "7%",
    },
    {
      title: "Tên vị trí",
      dataIndex: "tenViTri",
      render: (_, e) => {
        return <p>{e.tenViTri}</p>;
      },
      width: "10%",
    },
    {
      title: "Tỉnh thành",
      dataIndex: "tinhThanh",
      render: (_, e) => {
        return <p>{e.tinhThanh}</p>;
      },
      width: "20%",
    },
    {
      title: "Quốc gia",
      dataIndex: "quocGia",
      render: (_, e) => {
        return <p>{e.quocGia}</p>;
      },
      width: "20%",
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      render: (_, e) => {
        return <img src={e.hinhAnh} alt="" className="" />;
      },
      width: "20%",
    },
    {
      title: "Hành động",
      dataIndex: "action",
      render: (_, e) => {
        return (
          <div>
            {
              <div>
                <button
                  type="button"
                  className="focus:outline-none text-black bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900"
                >
                  Sửa
                </button>
                <button
                  type="button"
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                  onClick={() => {
                    if (window.confirm("Bạn có chắc muốn xóa vị trí này")) {
                      dispatch(deleteLocationThunk(e.id));
                    }
                  }}
                >
                  Xóa
                </button>
              </div>
            }
          </div>
        );
      },
      width: "20%",
    },
  ];
  const data = locationList;
  const onChange: TableProps<location>["onChange"] = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log("params", pagination, filters, sorter, extra);
  };
  return (
    <div>
      <Button className="!font-700 !bg-gray-300 mb-10">Thêm phòng</Button>
      <Search
        placeholder="input search text"
        enterButton="Tìm kiếm"
        size="large"
        suffix={suffix}
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey="id"
      />
    </div>
  );
};

export default AdminQuanLyThongTinViTri;
