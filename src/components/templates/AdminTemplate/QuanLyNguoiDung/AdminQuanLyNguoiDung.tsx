import { Input, Table } from "antd";
import type { ColumnsType, TableProps } from "antd/es/table";
import { AudioOutlined } from "@ant-design/icons";
import { Button } from "components/ui";
import { RootState, useAppDispatch } from "store";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { deleteUserThunk, getAllUserThunk } from "store/quanLyNguoiDung/thunk";
import { UserInfo } from "types";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";

export const AdminQuanLyNguoiDung = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const { allUser } = useSelector((state: RootState) => state.quanLyNguoiDung);

  useEffect(() => {
    dispatch(getAllUserThunk());
  }, [dispatch]);

  // table antd
  const columns: ColumnsType<UserInfo> = [
    {
      title: "Id",
      dataIndex: "id",
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend"],
      width: "5%",
    },
    {
      title: "Name",
      dataIndex: "name",
      render: (_, user) => {
        return <p>{user.name}</p>;
      },
      width: "20%",
    },
    {
      title: "Email",
      dataIndex: "email",
      render: (_, user) => {
        return <p>{user.email}</p>;
      },
      width: "25%",
    },
    {
      title: "Role",
      dataIndex: "role",
      render: (_, user) => {
        return <p>{user.role}</p>;
      },
      width: "10%",
    },
    {
      dataIndex: "",
      render: (_, user) => {
        return (
          <div>
            {
              <div>
                <button
                  type="button"
                  className="focus:outline-none text-black bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900"
                >
                  Sửa
                </button>
                <button
                  type="button"
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                  onClick={() => {
                    if (window.confirm("Bạn có chắc muốn xóa user này")) {
                      dispatch(deleteUserThunk(user.id));
                    }
                  }}
                >
                  Xóa
                </button>
              </div>
            }
          </div>
        );
      },
      width: "20%",
    },
  ];
  const data = allUser;
  const onChange: TableProps<UserInfo>["onChange"] = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  // search antd
  const { Search } = Input;
  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#1677ff",
      }}
    />
  );
  const onSearch = () => {
    const data = allUser.filter((item) => {
      return item.id;
    });
    console.log("data:", data);
    return data;
  };

  return (
    <div>
      <Button
        className="!font-700 !bg-gray-300 mb-10"
        onClick={() => {
          navigate(PATH.addUser);
        }}
      >
        Thêm Quản Trị Viên
      </Button>
      <Search
        placeholder="input search text"
        enterButton="Tìm kiếm"
        size="large"
        suffix={suffix}
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
};

export default AdminQuanLyNguoiDung;
