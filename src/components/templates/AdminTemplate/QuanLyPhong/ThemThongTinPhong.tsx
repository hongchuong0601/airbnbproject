// import { useState } from "react";
import { Input } from "components/ui";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
// import { Button, Form, Input, Radio, Select } from "antd";
import { useAppDispatch } from "store";
import { postRoomListThunk } from "store/quanLyPhong/thunk";

export const ThemThongTinPhong = () => {
  // const [componentSize, setComponentSize] = useState("default");
  const dispatch = useAppDispatch();

  const {
    handleSubmit,
    register,
    // formState: { errors },
  } = useForm({
    mode: "onChange",
  });

  const onSubmit = async (value) => {
    console.log("value", value);
    dispatch(postRoomListThunk())
      .unwrap()
      .then(() => {
        toast.success("Thêm phòng thành công");
      });
  };

  // const onFormLayoutChange = ({ size }) => {
  //   setComponentSize(size);
  // };

  return (
    <form className="pt-[10px] pb-[20px] " onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-black text-xl font-600">Thêm Phòng</h2>
      <div className="grid grid-cols-2 gap-10">
        <div className="mt-30">
          <Input
            register={register}
            name="id"
            // error={errors?.id?.message}
            placeholder="ID"
            type="number"
          />
        </div>
        <div className="mt-30">
          <Input
            register={register}
            name="tenPhong"
            // error={errors?.id?.message}
            placeholder="Tên phòng"
            type="text"
          />
        </div>
      </div>
      <div className="grid grid-cols-2 gap-10">
        <div className="mt-30">
          <Input
            register={register}
            name="khach"
            // error={errors?.id?.message}
            placeholder="Khách"
            type="number"
          />
        </div>
        <div className="mt-30">
          <Input
            register={register}
            name="phongNgu"
            // error={errors?.id?.message}
            placeholder="Phòng ngủ"
            type="number"
          />
        </div>
      </div>
      <div className="grid grid-cols-2 gap-10">
        <div className="mt-30">
          <Input
            register={register}
            name="giuong"
            // error={errors?.id?.message}
            placeholder="Giường"
            type="number"
          />
        </div>
        <div className="mt-30">
          <Input
            register={register}
            name="phongTam"
            // error={errors?.id?.message}
            placeholder="Phòng tắm"
            type="number"
          />
        </div>
      </div>
      <div className="grid grid-cols-2 gap-10">
        <div className="mt-30">
          <Input
            register={register}
            name="moTa"
            // error={errors?.id?.message}
            placeholder="Mô tả"
            type="text"
          />
        </div>
        <div className="mt-30">
          <Input
            register={register}
            name="giaTien"
            // error={errors?.id?.message}
            placeholder="Giá tiền"
            type="number"
          />
        </div>
      </div>

      <div className="grid grid-cols-3 gap-10">
        <div className="mt-30">
          <select
            name="mayGiat"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Máy giặt
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
        <div className="mt-30">
          <select
            name="banLa"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Bàn là
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
        <div className="mt-30">
          <select
            name="tivi"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Tivi
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
      </div>
      <div className="grid grid-cols-3 gap-10">
        <div className="mt-30">
          <select
            name="dieuHoa"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Máy điều hòa
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
        <div className="mt-30">
          <select
            name="wifi"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Wifi
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
        <div className="mt-30">
          <select
            name="bep"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Nhà bếp
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
      </div>
      <div className="grid grid-cols-3 gap-10">
        <div className="mt-30">
          <select
            name="doXe"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Chổ để xe
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
        <div className="mt-30">
          <select
            name="hoBoi"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Hồ bơi trong nhà
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
        <div className="mt-30">
          <select
            name="banUi"
            defaultValue={"DEFAULT"}
            className="outline-none block w-full p-12 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            // {...register("gender", {
            //   required: "Vui lòng chọn giới tính",
            // })}
            // {...register("gender")}
          >
            <option value="DEFAULT" disabled>
              Bàn ủi
            </option>
            <option value="true">Có</option>
            <option value="false">Không</option>
          </select>
          {/* {errors?.gender && (
          <p className="text-red-500">{errors?.gender?.message as string}</p>
        )} */}
        </div>
      </div>
      <div className="grid grid-cols-2 gap-10">
        <div className="mt-30">
          <Input
            register={register}
            name="maViTri"
            // error={errors?.id?.message}
            placeholder="Vị trí"
            type="number"
          />
        </div>
        <div className="mt-30">
          <Input
            register={register}
            name="hinhAnh"
            // error={errors?.id?.message}
            placeholder="Hình ảnh"
            type="file"
          />
        </div>
      </div>
    </form>
  );
};

export default ThemThongTinPhong;
