import { useEffect } from "react";
import { Button } from "components/ui";
import { Input, Table } from "antd";
import { AudioOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { Room } from "types";
import type { ColumnsType, TableProps } from "antd/es/table";
import { deleteRoomThunk, getRoomListThunk } from "store/quanLyPhong/thunk";
// import { deleteUserThunk } from "store/quanLyNguoiDung/thunk";

export const AdminQuanLyPhong = () => {
  const dispatch = useAppDispatch();

  const { roomList } = useSelector((state: RootState) => state.quanLyPhongApi);
  console.log("roomList:", roomList);
  useEffect(() => {
    dispatch(getRoomListThunk());
  }, [dispatch]);
  // search antd
  const { Search } = Input;
  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#1677ff",
      }}
    />
  );
  const onSearch = (value, _e, info) => console.log(info?.source, value);

  // table antd
  const columns: ColumnsType<Room> = [
    {
      title: "Id",
      dataIndex: "id",
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend"],
      width: "7%",
    },
    {
      title: "Mã vị trí",
      dataIndex: "maViTri",
      render: (_, room) => {
        return <p>{room.maViTri}</p>;
      },
      width: "10%",
    },
    {
      title: "Tên phòng",
      dataIndex: "tenPhong",
      render: (_, room) => {
        return <p>{room.tenPhong}</p>;
      },
      width: "20%",
    },
    {
      title: "Mô tả",
      dataIndex: "moTa",
      render: (_, room) => {
        return <p>{room.moTa.substring(0, 50)}...</p>;
      },
      width: "20%",
    },
    {
      title: "Giá tiền",
      dataIndex: "giaTien",
      render: (_, room) => {
        return <p>$ {room.giaTien}</p>;
      },
      width: "7%",
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      render: (_, room) => {
        return <img src={room.hinhAnh} alt="" className="" />;
      },
      width: "20%",
    },
    {
      title: "Hành động",
      dataIndex: "role",
      render: (_, room) => {
        return (
          <div>
            {
              <div>
                <button
                  // type="button"
                  className="focus:outline-none text-black bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900"
                >
                  Sửa
                </button>
                <button
                  // type="button"
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                  onClick={() => {
                    if (window.confirm("Bạn có chắc muốn xóa phòng này")) {
                      dispatch(deleteRoomThunk(room.id));
                    }
                  }}
                >
                  Xóa
                </button>
              </div>
            }
          </div>
        );
      },
      width: "20%",
    },
  ];
  const data = roomList;
  const onChange: TableProps<Room>["onChange"] = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log("params", pagination, filters, sorter, extra);
  };
  return (
    <div>
      <Button className="!font-700 !bg-gray-300 mb-10">Thêm phòng</Button>
      <Search
        placeholder="input search text"
        enterButton="Tìm kiếm"
        size="large"
        suffix={suffix}
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey="id"
      />
    </div>
  );
};

export default AdminQuanLyPhong;
