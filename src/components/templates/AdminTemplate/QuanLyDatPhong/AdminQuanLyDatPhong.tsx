import { useEffect } from "react";
import { Button } from "components/ui";
import { Input, Table } from "antd";
import { AudioOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { AllBooking } from "types";
import type { ColumnsType, TableProps } from "antd/es/table";
import {
  deleteUserBookingThunk,
  getAllBookingThunk,
} from "store/quanLyDatPhong/thunk";

export const AdminQuanLyDatPhong = () => {
  const dispatch = useAppDispatch();
  const { allBookingList } = useSelector(
    (state: RootState) => state.quanLyDatPhong
  );
  console.log("allBookingList:", allBookingList);
  useEffect(() => {
    dispatch(getAllBookingThunk());
  }, [dispatch]);
  // search antd
  const { Search } = Input;
  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: "#1677ff",
      }}
    />
  );
  const onSearch = (value, _e, info) => console.log(info?.source, value);

  // table antd
  const columns: ColumnsType<AllBooking> = [
    {
      title: "Id",
      dataIndex: "id",
      sorter: (a, b) => a.id - b.id,
      sortDirections: ["descend"],
      width: "7%",
    },
    {
      title: "Mã phòng",
      dataIndex: "maPhong",
      render: (_, booking) => {
        return <p>{booking.maPhong}</p>;
      },
      width: "10%",
    },
    {
      title: "Ngày đến",
      dataIndex: "ngayDen",
      render: (_, booking) => {
        return <p>{booking.ngayDen}</p>;
      },
      width: "10%",
    },
    {
      title: "Ngày đi",
      dataIndex: "ngayDi",
      render: (_, booking) => {
        return <p>{booking.ngayDi}</p>;
      },
      width: "10%",
    },
    {
      title: "Số lượng khách",
      dataIndex: "soLuongKhach",
      render: (_, booking) => {
        return <p>{booking.soLuongKhach}</p>;
      },
      width: "10%",
    },
    {
      title: "Mã người dùng",
      dataIndex: "maNguoiDung",
      render: (_, booking) => {
        return <p>{booking.maNguoiDung}</p>;
      },
      width: "10%",
    },
    {
      title: "Hành động",
      dataIndex: "action",
      render: (_, e) => {
        return (
          <div>
            {
              <div>
                <button
                  type="button"
                  className="focus:outline-none text-black bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900"
                >
                  Sửa
                </button>
                <button
                  type="button"
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                  onClick={() => {
                    if (window.confirm("Bạn có chắc muốn xóa Booking này"))
                      dispatch(deleteUserBookingThunk(e.id));
                  }}
                >
                  Xóa
                </button>
              </div>
            }
          </div>
        );
      },
      width: "20%",
    },
  ];
  const data = allBookingList;
  const onChange: TableProps<AllBooking>["onChange"] = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log("params", pagination, filters, sorter, extra);
  };
  return (
    <div>
      <Button className="!font-700 !bg-gray-300 mb-10">Thêm đặt phòng</Button>
      <Search
        placeholder="input search text"
        enterButton="Tìm kiếm"
        size="large"
        suffix={suffix}
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
};

export default AdminQuanLyDatPhong;
