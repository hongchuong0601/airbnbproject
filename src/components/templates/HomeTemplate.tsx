import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { useEffect } from "react";
import { getRoomListThunk } from "store/quanLyPhong/thunk";
import { generatePath } from "react-router-dom";
import { PATH } from "constant";

export const HomeTemplate = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const { roomList } = useSelector((state: RootState) => state.quanLyPhongApi);
  console.log("roomList:", roomList);

  useEffect(() => {
    dispatch(getRoomListThunk());
  }, [dispatch]);

  return (
    <div className="grid grid-cols-4 gap-20 max-[900px]:grid-cols-3 max-[650px]:grid-cols-2 max-[450px]:grid-cols-1 ">
      {roomList.map((room) => {
        const path = generatePath(PATH.detail, {
          roomId: room.id,
        });

        return (
          <div className="" key={room.id}>
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-white h-300  max-[1050px]:h-330 max-[900px]:h-300 max-[700px]:h-330 max-[650px]:h-300 max-[450px]:h-250 max-[300px]:h-200">
              <div className="">
                <img
                  className="rounded-t-lg cursor-pointer max-w-full h-150 max-[300px]:h-100 max-[200px]:h-70"
                  src={room.hinhAnh}
                  alt="example"
                />
              </div>
              <div className="p-5">
                <h5 className="mb-2 text-sm font-bold tracking-tight  text-black cursor-pointer max-[900px]:text-15 max-[650px]:text-15 max-[250px]:text-10">
                  {room.tenPhong}
                </h5>

                <p className="mb-3 font-normal text-gray-700 cursor-pointer max-[900px]:text-15 max-[450px]:text-10 max-[650px]:text-15">
                  {room.moTa.substring(0, 50)}...
                </p>
              </div>
            </div>
            <div className="">
              <button
                type="button"
                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 flex justify-center items-center max-[900px]:text-10 max-[900px]:p-0.5"
                onClick={() => {
                  navigate(path);
                }}
              >
                View More
                <svg
                  className="w-16 h-16 ml-10"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 14 10"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M1 5h12m0 0L9 1m4 4L9 9"
                  />
                </svg>
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default HomeTemplate;
