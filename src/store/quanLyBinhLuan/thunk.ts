import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyBinhLuanService } from "service";

export const getBinhLuanPhongThunk = createAsyncThunk(
    "quanLyBinhLuan/getBinhLuanPhong",
    async (roomId: string, {rejectWithValue})=>{
        try {
            const data = await quanLyBinhLuanService.getBinhLuanPhong(roomId)
            return data.data.content
        } catch (error) {
            return rejectWithValue(error)
        }
    }
)