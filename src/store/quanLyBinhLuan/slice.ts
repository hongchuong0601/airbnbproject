import { createSlice } from "@reduxjs/toolkit";
import { binhLuanPhong } from "types";
import { getBinhLuanPhongThunk } from "./thunk";

type QuanLyBinhLuanInitialState = {
    binhLuanPhong: binhLuanPhong[], 
}

const initialState: QuanLyBinhLuanInitialState  = {
    binhLuanPhong: [],
}

const quanLyBinhLuanSlice = createSlice({
    name: 'quanLyBinhLuan',
    initialState,
    reducers: {},

    extraReducers: (builder)=>{
        builder.addCase(getBinhLuanPhongThunk.fulfilled, (state, {payload})=>{
            state.binhLuanPhong = payload
        })
    }
})

export const {reducer: quanLyBinhLuanReducer, actions: quanLyBinhLuanActions} = quanLyBinhLuanSlice