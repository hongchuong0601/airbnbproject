import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhongService } from "service";

export const getRoomListThunk = createAsyncThunk(
  "quanLyPhong/getRoomListThunk",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyPhongService.getRoomList();
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getRoomDetailThunk = createAsyncThunk(
  "quanLyPhong/getRoomDetailThunk",
  async (roomId: string, { rejectWithValue }) => {
    try {
      const data = await quanLyPhongService.getRoomDetail(roomId);
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const postRoomListThunk = createAsyncThunk(
  "quanLyPhong/postRoomListThunk",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyPhongService.postRoomList();
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);


export const deleteRoomThunk = createAsyncThunk(
  'quanLyPhong/deleteRoom',
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async (id: any, {rejectWithValue, dispatch}) => {
    try {
      const data = await quanLyPhongService.deleteRoom(id);
      alert("Xóa thành công");
      dispatch(getRoomListThunk())
      return data.data.content
    } catch (error) {
      return rejectWithValue(error)
    }
  }
)