import { createSlice } from "@reduxjs/toolkit";
import { Room, RoomDetail } from "types";
import {
  deleteRoomThunk,
  getRoomDetailThunk,
  getRoomListThunk,
} from "./thunk";

type QuanLyPhongInitialState = {
  roomList: Room[];
  detailList?: RoomDetail;
  deleteRoom?: RoomDetail;
};

const initialState: QuanLyPhongInitialState = {
  roomList: [],
  detailList: {
    id: 0,
    tenPhong: "",
    khach: 0,
    phongNgu: 0,
    giuong: 0,
    phongTam: 0,
    moTa: "",
    giaTien: 0,
    mayGiat: false,
    banLa: false,
    tivi: false,
    dieuHoa: false,
    wifi: false,
    bep: false,
    doXe: false,
    hoBoi: false,
    banUi: false,
    maViTri: 0,
    hinhAnh: "",
  },

};

const quanLyPhongSlice = createSlice({
  name: "quanLyPhong",
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      .addCase(getRoomListThunk.fulfilled, (state, { payload }) => {
        state.roomList = payload;
      })

      .addCase(getRoomDetailThunk.fulfilled, (state, { payload }) => {
        state.detailList = payload;
      })

      .addCase(deleteRoomThunk.fulfilled, (state, {payload})=>{
        state.deleteRoom = payload
      })
  },
});

export const { reducer: quanLyPhongReducer, actions: quanLyPhongActions } =
  quanLyPhongSlice;
