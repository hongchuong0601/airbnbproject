import { useDispatch } from "react-redux";
import { rootReducer } from "./rootReducer";
import {configureStore} from '@reduxjs/toolkit'
import { getInfoUserThunk } from "./quanLyNguoiDung/thunk";







export const store = configureStore({
    reducer: rootReducer,
})

store.dispatch(getInfoUserThunk())

export type RootState = ReturnType<(typeof store)["getState"]>;

type AppDispatch = (typeof store)["dispatch"]

export const useAppDispatch: () => AppDispatch = useDispatch;