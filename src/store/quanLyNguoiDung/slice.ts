import { createSlice } from "@reduxjs/toolkit";
import { UserInfo } from "types";
import {
  deleteUserThunk,
  getAllUserThunk,
  getInfoUserThunk,
  updateUserThunk,
} from "./thunk";

type quanLyNguoiDungInitialSlice = {
  userInfo?: UserInfo;
  token?: string;
  isUpdatingUser?: boolean;
  allUser?: UserInfo[];
  delUser?: UserInfo;
};

const initialState: quanLyNguoiDungInitialSlice = {
  token: localStorage.getItem("token"),
  isUpdatingUser: false,
  allUser: [],
  
};

const quanLyNguoiDungSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {
    logOut: (state) => {
      (state.userInfo = undefined), localStorage.removeItem("Id");
      localStorage.removeItem("token");
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(getInfoUserThunk.fulfilled, (state, { payload }) => {
        console.log("payload InfoUser", payload);
        if (payload) {
          state.userInfo = payload;
        }
      })

      .addCase(updateUserThunk.pending, (state) => {
        state.isUpdatingUser = true;
      })
      .addCase(updateUserThunk.fulfilled, (state) => {
        state.isUpdatingUser = false;
      })
      .addCase(updateUserThunk.rejected, (state) => {
        state.isUpdatingUser = false;
      })

      .addCase(getAllUserThunk.fulfilled, (state, { payload }) => {
        payload = payload.map((item) => {
          return { ...item, key: item.id };
        });
        state.allUser = payload;
      })

      .addCase(deleteUserThunk.fulfilled, (state, { payload }) => {
        state.delUser = payload;
      })
  },
});

export const {
  reducer: quanLyNguoiDungReducer,
  actions: quanLyNguoiDungActions,
} = quanLyNguoiDungSlice;
