import { createAsyncThunk } from "@reduxjs/toolkit";
import { AccountSchemaType } from "schema";
// import { AccountSchemaType } from "schema";
import { quanLyNguoiDungService } from "service";


export const getInfoUserThunk = createAsyncThunk(
  "quanLyNguoiDung/getUserInfo",
  async (_, { rejectWithValue }) => {
    try {
      const id = localStorage.getItem("Id");
      if (id) {
        const data = await quanLyNguoiDungService.getUserInfo(id);
        return data.data.content;
      }
      return undefined
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const updateUserThunk = createAsyncThunk(
  "quanLyNguoiDung/updateUserThunk",
  async ( payload: AccountSchemaType, {rejectWithValue, dispatch} )=>{
    try {
      const id = localStorage.getItem('Id')
      await quanLyNguoiDungService.updateUser(id ,payload)
      dispatch(getInfoUserThunk())
    } catch (error) {
     return rejectWithValue(error)
      
    }
  }
)

export const getAllUserThunk = createAsyncThunk(
  "quanLyNguoiDung/getAllUserThunk",
  async (_, {rejectWithValue})=>{
    try {
      const data = await quanLyNguoiDungService.getAllUser()
      return data.data.content
    } catch (error) {
      return rejectWithValue(error)
    }
  }
)


export const deleteUserThunk = createAsyncThunk(
  "quanLyNguoiDung/deleteUser",
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async (delId: any, {rejectWithValue, dispatch}) => {
    try {
      const data = await quanLyNguoiDungService.deleteUser(delId); 
      alert('Xóa thành công')
      dispatch(getAllUserThunk())
      return data.data.content
    } catch (error) {
      return rejectWithValue(error)
    }
  }
)

export const postAvatarThunk = createAsyncThunk(
  'quanLyNguoiDung/postAvatarThunk',
  async (_, {rejectWithValue}) => {
    try {
      const data = await quanLyNguoiDungService.postAvatar();
      return data.data.content
    } catch (error) {
      return rejectWithValue(error)
    }
  }
)
