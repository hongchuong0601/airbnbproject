import { createAsyncThunk } from "@reduxjs/toolkit";
import { SigninSchemaType, SignupSchemaType } from "schema";
import { quanLyAuthApiService } from "service/quanLyAuthApi";

export const signupThunk = createAsyncThunk(
  "quanLyAuthApi/signupThunk",
  async (payload: SignupSchemaType, { rejectWithValue }) => {
    try {
      const data = await quanLyAuthApiService.register(payload);
      return data.data.content;
    } catch (error) {
      console.log("error:", error)
      return rejectWithValue(error);
      
    }
  }
);

export const signinThunk = createAsyncThunk(
  "quanLyAuthApi/signin",
  async (payload: SigninSchemaType, {rejectWithValue})=>{
    try {
      const data = await quanLyAuthApiService.login(payload)
      return data.data.content
    } catch (error) {
      return rejectWithValue(error)
    }
  }
)




