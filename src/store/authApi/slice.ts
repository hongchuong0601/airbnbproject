import { createSlice } from "@reduxjs/toolkit";
import { signinThunk } from "./thunk";
import { User } from "types";

type quanLyAuthApiInitialState = {
  user?: User;
  token?: string;
  id?: string;
};

const initialState: quanLyAuthApiInitialState = {
  token: localStorage.getItem("token"),
  id: localStorage.getItem('Id')
};

const quanLyAuthSlice = createSlice({
  name: "quanLyAuthApi",
  initialState,
  reducers: {
    logOut: (state) => {
      state.user = undefined,
      localStorage.removeItem("Id")
      localStorage.removeItem("token")
    }
  },
  extraReducers: (builder) => {
    builder.addCase(signinThunk.fulfilled, (state, { payload }) => {
      console.log("payload fulfilled", payload);
      state.user = payload;
      if (payload) {
        localStorage.setItem("Id", JSON.stringify(payload.user.id));
        localStorage.setItem("token", payload.token)
      }
    });
  },
});

export const { reducer: quanLyAuthApiReducer, actions: quanLyAuthApiActions } =
  quanLyAuthSlice;
