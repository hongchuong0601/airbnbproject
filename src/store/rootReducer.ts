import { combineReducers } from "@reduxjs/toolkit";
import { quanLyAuthApiReducer } from "./authApi/slice";
import { quanLyPhongReducer } from "./quanLyPhong/slice";
import { quanLyNguoiDungReducer } from "./quanLyNguoiDung/slice";
import { quanLyBinhLuanReducer } from "./quanLyBinhLuan/slice";
import { quanLyDatPhongReducer } from "./quanLyDatPhong/slice";
import { quanLyPhongTheoViTriReducer } from "./quanLyPhongTheoViTri/slice";

export const rootReducer = combineReducers({
    quanLyAuthApi: quanLyAuthApiReducer,
    quanLyPhongApi: quanLyPhongReducer,
    quanLyNguoiDung: quanLyNguoiDungReducer,
    quanLyBinhLuan: quanLyBinhLuanReducer,
    quanLyDatPhong: quanLyDatPhongReducer,
    quanLyPhongTheoViTri: quanLyPhongTheoViTriReducer,
})