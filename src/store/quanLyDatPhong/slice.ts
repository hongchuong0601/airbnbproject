import { createSlice } from "@reduxjs/toolkit";
import { AllBooking, UserBooking } from "types";
import { deleteUserBookingThunk, getAllBookingThunk, getUserBookingThunk } from "./thunk";



type QuanLyDatPhongInitialState = {
    allBookingList: AllBooking[];
    userBooking: UserBooking[];
   
}

const initialState: QuanLyDatPhongInitialState  = {
    allBookingList: [],
    userBooking: [],
   
}

const quanLyDatPhongSlice = createSlice({
    name: 'quanLyDatPhong',
    initialState,
    reducers: {},

    extraReducers: (builder) => {
        builder
        .addCase(getAllBookingThunk.fulfilled, (state, {payload})=>{
            payload = payload.map((value) => {
                return {...value, key: value.id}
            })
            state.allBookingList = payload
        })
        .addCase(getUserBookingThunk.fulfilled, (state, {payload})=>{
            payload= payload.map((value) => {
                return {...value, key: value.id}
            })
            state.userBooking = payload
        })
        .addCase(deleteUserBookingThunk.fulfilled, (state, {payload}) => {
            state.allBookingList = payload
        })
    }
})

export const {reducer: quanLyDatPhongReducer, actions: quanLyDatPhongActions} = quanLyDatPhongSlice