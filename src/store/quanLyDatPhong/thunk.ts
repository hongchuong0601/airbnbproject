import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyDatPhongService } from "service";

export const getAllBookingThunk = createAsyncThunk(
  "quanLyDatPhong/getAllBookingThunk",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyDatPhongService.getAllBooking();
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getUserBookingThunk = createAsyncThunk(
  "quanLyDatPhong/getUserBookingThunk",
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async (maNguoiDung: any, { rejectWithValue }) => {
    try {
      const data = await quanLyDatPhongService.getUserBooking(maNguoiDung);
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const postUserBookingThunk = createAsyncThunk(
  "quanLyDatPhong/postUserBookingThunk",
  async (payload, { rejectWithValue }) => {
    try {
      const data = await quanLyDatPhongService.postUserBooking(payload);
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const deleteUserBookingThunk = createAsyncThunk(
  "quanLyDatPhong/deleteUserBooking",
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async (id: any, { rejectWithValue, dispatch }) => {
    try {
      const data = await quanLyDatPhongService.deleteUserBooking(id);
      alert("Xóa thành công");
      dispatch(getAllBookingThunk());
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
