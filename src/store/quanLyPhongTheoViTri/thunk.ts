import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhongTheoViTriService } from "service";

export const getAllLocationThunk = createAsyncThunk(
    "quanLyPhongTheoViTri/getAllLocation",
    async (_, {rejectWithValue}) => {
        try {
            const data = await quanLyPhongTheoViTriService.getAllLocation();
            return data.data.content
        } catch (error) {
            return rejectWithValue(error)
        }
    }
);

export const deleteLocationThunk = createAsyncThunk(
    "quanLyPhongTheoViTri/deleteLocationThunk",
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async (id: any, {rejectWithValue, dispatch}) => {
        try {
            const data = await quanLyPhongTheoViTriService.deleteLocation(id)
            alert("Xóa thành công")
            dispatch(getAllLocationThunk())
            return data.data.content
        } catch (error) {
            return rejectWithValue(error)
        }
    }
)

