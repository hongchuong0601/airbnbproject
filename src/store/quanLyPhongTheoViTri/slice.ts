import { createSlice } from "@reduxjs/toolkit";
import { deleteLocationThunk, getAllLocationThunk } from "./thunk";
import { location } from "types";

type QuanLyPhongTheoViTriInitialState = {
    locationList?: location[];
}

const initialState: QuanLyPhongTheoViTriInitialState = {
    locationList: [],
}

const quanLyPhongTheoViTriSlice = createSlice({
    name: 'quanLyPhongTheoViTri',
    initialState,
    reducers: {},

    extraReducers: (builder) => {
        builder
        .addCase(getAllLocationThunk.fulfilled, (state, {payload})=> {
            state.locationList = payload
        })
        .addCase(deleteLocationThunk.fulfilled, (state, {payload}) => {
            state.locationList = payload
        })
    }
})

export const {reducer: quanLyPhongTheoViTriReducer, actions: quanLyPhongTheoViTriActions} = quanLyPhongTheoViTriSlice