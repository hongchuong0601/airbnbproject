import { z } from "zod";

export const AccountSchema = z.object({
  email: z
    .string()
    .nonempty("Vui lòng nhập email")
    .email("Vui lòng nhập đúng email"),
  phone: z
    .string()
    .nonempty("Vui lòng nhập số điện thoại")
    .max(20, "ID tối đa 12 ký tự")
    .min(6, "ID tối thiểu 9 ký tự"),
  birthday: z.string().nonempty("Vui lòng nhập ngày sinh"),
  name: z.string().nonempty("Vui lòng nhập họ tên"),
  gender: z.string() || z.boolean(),
  role: z.string().nonempty("Vui lòng nhập role"),
});

export type AccountSchemaType = z.infer<typeof AccountSchema>;
