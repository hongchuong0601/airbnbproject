import { z } from "zod";

export const SigninSchema = z.object({
  email: z
    .string()
    .nonempty("Vui lòng nhập email")
    .email("Vui lòng nhập đúng email"),
  password: z.string().nonempty("Vui lòng nhập mật khẩu"),
});

export type SigninSchemaType = z.infer<typeof SigninSchema>;
