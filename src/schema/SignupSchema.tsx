import { z } from "zod";

export const SignupSchema = z.object({
  id: z.string().nonempty("Vui lòng nhập số").max(10, "ID không quá 10 ký tự"),
  password: z.string().nonempty("Vui lòng nhập mật khẩu"),
  email: z
    .string()
    .nonempty("Vui lòng nhập email")
    .email("Vui lòng nhập đúng email"),
  phone: z
    .string()
    .nonempty("Vui lòng nhập số điện thoại")
    .max(20, "Số điện thoại tối đa 12 ký tự")
    .min(6, "Số điện thoại tối thiểu 9 ký tự"),
  birthday: z.string().nonempty("Vui lòng nhập ngày sinh"),
  name: z.string().nonempty("Vui lòng nhập họ tên"),
  gender: z.string() || z.boolean(),
  role: z.string().nonempty("Vui lòng nhập role"),
});

export type SignupSchemaType = z.infer<typeof SignupSchema>;
